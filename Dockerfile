FROM  ubuntu:20.04

ENV TZ America/New_York

RUN echo "Rebuild"

# Core Tools
RUN apt-get update  \
    && apt-get dist-upgrade -y \
    && DEBIAN_FRONTEND=noninteractive apt-get  install -y --no-install-recommends \
        vim-tiny \
        less \
        net-tools \
        inetutils-ping \
        curl \
        git \
        telnet \
        nmap \
        socat \
        wget \
        sudo \
        sqlite3 \
        gdal-bin \
        libgdal-dev \
        build-essential \
        gdebi-core \
        locales \
        tzdata \
        gnupg2 \
        apt-utils \
        ssh \
        software-properties-common \
        libmagick++-dev


# Supervisord 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
       supervisor && \
       mkdir -p /var/log/supervisor
CMD ["/usr/bin/supervisord", "-n"]

# set the locale so RStudio doesn't complain about UTF-8
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen en_US.utf8 \
    && /usr/sbin/update-locale LANG=en_US.UTF-8

# set the timezone
RUN ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime \
    && dpkg-reconfigure --frontend noninteractive tzdata

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive 

# we need TeX for the rmarkdown package in RStudio, and pandoc is also useful 
RUN apt-get update \
    && apt-get  install -y --no-install-recommends \
        texlive \
        texlive-base \
        texlive-latex-extra \
        texlive-pstricks \
        texlive-publishers \
        texlive-fonts-extra \
        texlive-humanities \
        lmodern \
        pandoc

# Libraries
RUN apt-get update \
    && apt-get  install -y --no-install-recommends \
        libopenblas-base \
        libcurl4-openssl-dev \
        libxml2-dev \
        libssl-dev \
        libfreetype6-dev \
        libudunits2-dev \
        libtiff-dev \
        libcairo2-dev \
        libxt-dev \
        libavfilter-dev \
		libopenmpi-dev

RUN apt install --no-install-recommends -y \
    software-properties-common \
	dirmngr
# add the signing key (by Michael Rutter) for these repos
# To verify key, run gpg --show-keys /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc 
# Fingerprint: 298A3A825C0D65DFD57CBB651716619E084DAB9
RUN wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | sudo tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
# add the R 4.0 repo from CRAN -- adjust 'focal' to 'groovy' or 'bionic' as needed
RUN add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
RUN apt install --no-install-recommends -y \
    r-base \
    r-base-dev \
    r-recommended \
    littler \
&& ln -s /usr/lib/R/site-library/littler/examples/install.r /usr/local/bin/install.r \
&& ln -s /usr/lib/R/site-library/littler/examples/install2.r /usr/local/bin/install2.r \
&& ln -s /usr/lib/R/site-library/littler/examples/installGithub.r /usr/local/bin/installGithub.r \
&& ln -s /usr/lib/R/site-library/littler/examples/testInstalled.r /usr/local/bin/testInstalled.r \
&& install.r docopt \
&& rm -rf /tmp/downloaded_packages/ /tmp/*.rds

# Install R-Studio server latest 
#RUN wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-2021.09.1-372-amd64.deb \
#    && DEBIAN_FRONTEND=noninteractive gdebi --n rstudio-server-2021.09.1-372-amd64.deb \
#    && rm rstudio-server-2021.09.1-372-amd64.deb

RUN wget https://s3.amazonaws.com/rstudio-ide-build/server/bionic/amd64/rstudio-server-2022.02.0-442-amd64.deb \  
    && DEBIAN_FRONTEND=noninteractive gdebi --n rstudio-server-2022.02.0-442-amd64.deb \
    && rm rstudio-server-2022.02.0-442-amd64.deb

RUN wget https://github.com/quarto-dev/quarto-cli/releases/download/v0.9.21/quarto-0.9.21-linux-amd64.deb \
    && DEBIAN_FRONTEND=noninteractive gdebi --n quarto-0.9.21-linux-amd64.deb \
    && rm quarto-0.9.21-linux-amd64.deb

ADD ./Rprofileconf /Rprofile_conf 
RUN ls -la  /Rprofile_conf/*
RUN mv /Rprofile_conf/Rprofile.site /usr/lib/R/etc/Rprofile.site
#RUN    rmdir /Rprofile_conf/
   
# R packages - infrastructure
RUN install2.r --error -s --deps TRUE \
    devtools \
    usethis \
    credentials \
    gitcreds \
    gert \
    renv \
    here \
    rcmdcheck \
    robotstxt \
    fs \
    reprex \
    sessioninfo

RUN installGithub.r \
    rstudio/gradethis \
    rstudio/learnr

# R packages - authoring
RUN installGithub.r \
    quarto-dev/quarto-r

RUN install2.r --error -s --deps TRUE BiocManager \
 && Rscript -e "BiocManager::install(c('graph', 'marray', 'affy', 'Biobase', 'limma'))"

# R packages - data
RUN install2.r --error -s -r "https://fivethirtyeightdata.github.io/drat/" fivethirtyeightdata ; \
    install2.r --error -s --deps TRUE \
    anyflights \
    openintro \
    babynames \
    datasauRus \
    fivethirtyeight \
    mosaicData \
    palmerpenguins \
    nycflights13 \
    schrute

RUN installGithub.r \
    rstudio-education/dsbox

# R packages - publishing
RUN install2.r --error -s --deps TRUE \
    rsconnect

# R packages - shiny
RUN install2.r --error -s --deps TRUE \
    shiny \
    shinyjs \
    shinythemes \
    shinydashboard \
    bslib \
    thematic \
    flexdashboard \
    miniUI

# R packages - tables
RUN install2.r --error -s --deps TRUE \
    DT \
    gt \
    kableExtra

# R packages - data analysis
RUN install2.r --error -s --deps TRUE \
    tidyverse \
    tidymodels \
    skimr \
    janitor \
    arm \
    forecast \
    forecTheta \
    splines2 \
    tidybayes \
    tidygraph \
    tidypredict \
    tidytext \
    stopwords \
    pairwiseCI \
    igraph \
    ggraph

# R packages - plotting
RUN install2.r --error -s --deps TRUE \
    gganimate \
    ggdendro \
    ggforce \
    ggformula \
    ggfortify \
    ggmap \
    ggraph \
    ggridges \
    ggthemes \
    gifski \
    magick \
    mapproj \
    maps \
    maptools \
    r2d3 \
    bayesplot \
    cowplot \
    patchwork \
    ragg \
    GGally \
    leaflet \
    leaflet.providers \
    hexbin

# R packages - spatial
RUN install2.r --error -s -r "https://nowosad.github.io/drat/" \
    spDataLarge ; \
    install2.r --error -s --deps TRUE \
    sf \
    sp \
    rgeos \
    rgdal \
    leaflet \
    raster \
    spData \
    spdep \
    spatialreg


RUN adduser --disabled-password --gecos "" --ingroup users guest 

# Config files
ADD ./conf /r-studio-configs 
RUN cd /r-studio-configs && \
    cp rserver.conf /etc/rstudio/ && \
    cp rsession.conf /etc/rstudio/ && \
    cp database.conf /etc/rstudio/ && \
    chown guest /etc/rstudio/* && \
    chown guest /var/log/supervisor
#RUN rm -rf /r-studio-configs

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y libgit2-dev texlive-xetex

#########
#
# if you need additional tools/libraries, add them here.
# also, note that we use supervisord to launch both the password
# initialize script and the RStudio server. If you want to run other processes
# add these to the supervisord.conf file
#
#########

RUN apt-get autoremove -y \
    && apt-get clean

# expose the RStudio IDE port
EXPOSE 8787 
RUN cp /r-studio-configs/supervisord.conf /etc/supervisor/supervisord.conf

USER guest
CMD [ "/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf" ]
